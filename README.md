# MTM-DevDay

## Getting things ready

* **Ruby version**
  * ruby 2.4.1
  * rails 5.1.6
* **System dependencies**
  * rvm (or other ruby version manager) is advised
  * docker
  * docker-compose
* **Configuration**
  * docker-compose up
  * bundle install
* **Database creation**
  * rails db:setup
* **Development**
  * `rails s`
  * go to http://localhost:3000
    * login with email: mail@example.com, password: password
  * go to http://localhost:3000/admin
    * login with email: admin@example.com, password: password
  * This is a known bug with ActiveAdmin and Devise which we just accept for this little testing setup


## Tasks

(Tasks from GDoc be here)
